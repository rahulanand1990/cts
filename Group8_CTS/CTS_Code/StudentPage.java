import java.lang.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class StudentPage extends JFrame
{
	JLabel ID,amount,amt;
	JTextField idno;
	JButton cancel,transact;
	JPanel panel;
	String shop;
	
	public void pullThePlug() 
	{
		WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
	}
	
	
	StudentPage(String s, String s1)
	{
		super("Student Authentication");
		shop = s1;
		System.out.println(shop);
		ID = new JLabel("Student ID No.");
		amount = new JLabel("Total Amount");
		amt = new JLabel(s);
		amt.setFont(new Font(null, Font.BOLD, 24));
		idno = new JTextField();
		idno.setFont(new Font(null, Font.PLAIN, 16));
		transact = new JButton("Transact");
		cancel = new JButton("Cancel");
		
		transact.addActionListener
		(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{	
							
				if(idno.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Please enter your ID no !!");
				}
				else
				{	
			        DatabaseHandler dbHandler = new DatabaseHandler(idno.getText(), amt.getText(),shop);
			        
			        int succ = dbHandler.writeData();
					
					if(succ==1)
			        	JOptionPane.showMessageDialog(null,"Transaction Complete !!");
					else if (succ==0)
						JOptionPane.showMessageDialog(null,"Transaction Failed !!");
					else if (succ==2)
						JOptionPane.showMessageDialog(null,"Transaction Failed due to insufficient balance !!");
	
					
					pullThePlug();
					MenuScreen menu = new MenuScreen(shop);
				}
			}
			
		}
		);
		
		cancel.addActionListener
		(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				//System.exit(0);
				//setVisible(false);
				pullThePlug();
				MenuScreen menu = new MenuScreen(shop);
			}
		}
		);
		
		panel = new JPanel(new GridLayout(3,2,10,10));
		panel.add(ID);
		panel.add(idno);
		panel.add(amount);
		panel.add(amt);
		panel.add(transact);
		panel.add(cancel);
		
		Container con = getContentPane();
		con.add(panel);
		
		setSize(400,400);
		setVisible(true);
		setLocationRelativeTo(null);
	}
}
		