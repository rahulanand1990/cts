import static org.junit.Assert.*;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class LoginTest {

	protected Login l,l1;
	
	public LoginTest()
	{
		l = new Login();

		l.user_text.setText("Monginis");
		l.pass_text.setText("Monginis");

		//l1 = new Login();
		//l1.user_text.setText("Nescafe");
		//l1.pass_text.setText("Nescafe1");

		
	}
	
@BeforeClass
	public static void setUpBeforeClass() throws Exception {
}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	
	}

	@Before
	public void setUp() throws Exception {
	
		
	}

	@After
	public void tearDown() throws Exception {
			
	}

	@Test
	public void testLogin() {
		
		String n = l.user_text.getText();
		String p = l.pass_text.getText();
			
//		String n1 = l1.user_text.getText();
//		String p1 = l1.pass_text.getText();
		

		Assert.assertEquals(n,"Monginis");
		Assert.assertEquals(p,"Monginis");
		
//		Assert.assertEquals(n1,"Nescafe");
//		Assert.assertEquals(p1,"Nescafe");

	}

	@Test
	public void testActionPerformed() {
		
	}

}
