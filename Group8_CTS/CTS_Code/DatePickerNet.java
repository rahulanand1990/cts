import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.prefs.BackingStoreException;

import javax.swing.*;


class DatePickerNet extends JFrame 
{
	String shop;
	boolean error = false;
	
	public long compareStringDates(String startDate, String endDate) throws ParseException
	{
		   
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );  
		java.util.Date d1 = sdf.parse(startDate);  
		java.util.Date d2 = sdf.parse(endDate);  
		   
		//System.out.println( "1. " + sdf.format( d1 ).toUpperCase() );  
		//System.out.println( "2. " + sdf.format( d2 ).toUpperCase() );  
		   
		return (d1.getTime() - d2.getTime());
		
	}
	
	public void pullThePlug() 
	{
		WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
	}
	
	public DatePickerNet(String s)
	{
	    shop = s;
	    System.out.println(s);
		JLabel label = new JLabel("       Start Date :");
        final JTextField text = new JTextField();
        text.setEditable(false);
        JButton b = new JButton("Choose from Calendar");
        JButton submitButton = new JButton("Submit");
        final JPanel rootPanel = new JPanel(new GridLayout(3,1,20,20)); 
        JPanel p = new JPanel(new GridLayout(1,4,20,0));
        p.add(label);
        p.add(text);
        p.add(b);
                     
        final JFrame f = new JFrame();                          
        
        getContentPane().add(p);
        
        b.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                        text.setText(new DatePicker1().setPickedDate());                                              
                        
                }
        });
        
        
        JLabel label1 = new JLabel("        End Date :");
        final JTextField text1 = new JTextField();
        text1.setEditable(false);
        JButton b1 = new JButton("Choose from Calendar");
        JPanel p1 = new JPanel(new GridLayout(1,3,20,0));
        p1.add(label1);
        p1.add(text1);
        p1.add(b1);
        //final JFrame f = new JFrame();

        b1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                        text1.setText(new DatePicker1().setPickedDate());
                }
        });
        
        JPanel p2 = new JPanel(new GridLayout(1,3,20,0));
        JButton logoutButton, backButton;
        logoutButton = new JButton("Logout");
        backButton = new JButton("Back");
        p2.add(backButton);
        p2.add(submitButton);
        p2.add(logoutButton);
        
        rootPanel.add(p);
        rootPanel.add(p1);
        rootPanel.add(p2);
        
        Container cont = getContentPane();
        cont.add(rootPanel);
        
        setSize(400, 400);
        setTitle("Date Picker");
        pack();
        setVisible(true);                         
        setLocationRelativeTo(null);

        submitButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				// TODO Auto-generated method stub
			
				pullThePlug();
				
				if(text.getText().equals("")|| text1.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null,"You cannot leave date field as empty !!!");
					error = true;
					pullThePlug();
					DatePickerNet picker = new DatePickerNet(shop);
				}
				
				long x = 0;
				try {
					if (error==false) 
						x = compareStringDates(text.getText(),text1.getText());					
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (x>0)
				{
					JOptionPane.showMessageDialog(null, "Start Date should be earlier than End Date");
					pullThePlug();
					DatePickerNet picker = new DatePickerNet(shop);
				}
				else if(error==false)
					try {
						
					ViewSales viewSales = new ViewSales(text.getText(),text1.getText(),shop);
					
					if(viewSales.rows==0)
					{
						JOptionPane.showMessageDialog(null, "No transactions done from " +  text.getText() + " to " + text1.getText());
						text.setText("");
						text1.setText("");
					}
					else
						viewSales.readDB();
					
					} catch (Exception e) {
					// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}
			
		});
        
        backButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
			
				pullThePlug();
				MenuScreen menuScreen = new MenuScreen(shop);
				
			}
		});

        
        logoutButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
			
				JOptionPane.showMessageDialog(null,"ThankYou for using CTS");
				System.exit(0);
				
			}
		});
        
	}
	
}