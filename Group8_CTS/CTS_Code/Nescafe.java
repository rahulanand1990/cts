import java.lang.*;

class Nescafe
{
	String shop_name;
	Item items[] = new Item[5];
	Nescafe(String shop)
	{
		shop_name = shop;
		items[0] = new Item("Maggi",20.00);
		items[1] = new Item("Pasta",25.00);
		items[2] = new Item("Iced Tea",18.00);
		items[3] = new Item("Frappe",20.00);
		items[4] = new Item("VegPuff",8.00);
	}
}
