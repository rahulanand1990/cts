import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.xml.transform.ErrorListener;

import java.sql.*;


public class ViewSales extends JFrame 
{
	Connection con;
	ResultSet rs;
	Statement st;
	String bn,bp,ba,startDate, endDate;
	String bid;
    int rows = 0;
	Object data1[][];
	JScrollPane scroller;
	JTable table;
	String shopName;
	JPanel rootPanel;
	JButton backButton, logoutButton, selDateButton;
	JLabel labelError;
	
	public void pullThePlug() 
	{
		WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
	}
	
	public void readDB() throws SQLException
	{
		data1=new Object[rows][4];
		
		Object[] Colheads={"Date","Time","ID", "Amount"};
		rs=st.executeQuery("SELECT * from " + shopName.toLowerCase() + "_transaction where TransactionDate between '"+ startDate + "' and '" + endDate + "'");
		//rs=st.executeQuery("SELECT * from nescafe_transaction");
		
		for(int i=0;i<rows;i++)
		{
			rs.next();
			for(int j=0;j<4;j++)
			{
				data1[i][j]=rs.getString(j+1);
			}
		}
		
		JTable table=new JTable(data1,Colheads);
		table.setRowHeight(30);
		
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
		table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
		table.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
		table.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
		
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
		
		JScrollPane jsp=new JScrollPane(table,v,h);
		
		//backButton.setBounds(100, 200, 80, 50);
		//jsp.setBounds(30, 20, 400, 160);
		
		rootPanel = new JPanel();
		rootPanel.add(jsp);
		
		JPanel bottomPanel = new JPanel(new GridLayout(1,3,50,0));
		bottomPanel.add(backButton);
		bottomPanel.add(selDateButton);
		bottomPanel.add(logoutButton);
		
		rootPanel.add(bottomPanel);
		
		getContentPane().add(rootPanel);
		setSize(550,550);
		//pack();
	    setVisible(true);
		setLocationRelativeTo(null);
		
	}
	
    public ViewSales(String s, String e, String shp) throws Exception
    {
    	shopName = shp;
    	startDate = s;
    	endDate = e; 
		backButton = new JButton("Back");			
		selDateButton = new JButton("Re-select Dates");
		logoutButton = new JButton("Logout");
    	
	    Container cp = getContentPane();
	   			
		String userName = "root";
		String password = "vishal";
		String url = "jdbc:mysql://localhost/CTS";
		Class.forName ("com.mysql.jdbc.Driver").newInstance ();
		con = DriverManager.getConnection (url, userName, password);
		System.out.println ("Database connection established");
	   	   	
		st = con.createStatement ();	//Creating Statement Object.
		rs=st.executeQuery("SELECT * from " + shopName.toLowerCase() + "_transaction where TransactionDate between '"+ startDate + "' and '" + endDate + "'");
		
		rs=st.getResultSet();
		
	    while(rs.next())
	    {
	    	bid = rs.getString(1);
	    	bn = rs.getString(2);
	    	bp = rs.getString(3);
	    	ba = rs.getString(4);
	    	rows++;    	
	    }  
	    		
	    //System.out.println("rows = " + rows);
	    
		logoutButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
			
				JOptionPane.showMessageDialog(null,"ThankYou for using CTS");
				System.exit(0);
				
			}
		});
        
		selDateButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
			
				pullThePlug();
				DatePickerNet dPicker = new DatePickerNet(shopName);
				
			}
		});
		
		backButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				pullThePlug();
				MenuScreen mScreen = new MenuScreen(shopName);
			}
		});
		
	}  
 }