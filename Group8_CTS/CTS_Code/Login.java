import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

class Login extends JFrame implements ActionListener
{
	JButton submit,clear;
	JPanel panel;
	JLabel uname,passwd;
	final JTextField user_text, pass_text;
	
	public void pullThePlug() 
	{
		WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
	}
	
	Login()
	{
		uname = new JLabel();
		uname.setText("          Username : ");
		user_text = new JTextField(20);
		
		passwd = new JLabel();
		passwd.setText("          Password : ");
		pass_text = new JPasswordField(20);
		
		submit = new JButton("Login");
		clear = new JButton("Clear");
		
		panel = new JPanel(new GridLayout(3,2,10,10));
		panel.add(uname);
		panel.add(user_text);
		panel.add(passwd);
		panel.add(pass_text);
		panel.add(submit);
		panel.add(clear);
		
		add(panel,BorderLayout.CENTER);
		
		submit.addActionListener(this);
		
		clear.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
			
				user_text.setText("");
				pass_text.setText("");
				
			}
		});
		
		setTitle("Vendor Login");
		
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		String val1 = user_text.getText();
		String val2 = pass_text.getText();
		
		if(val1.equals("Monginis") && val2.equals("Monginis"))
		{
			//this.setVisible(false);
			pullThePlug();
			MenuScreen page = new MenuScreen("Monginis");
			
		}
		else if(val1.equals("Nescafe") && val2.equals("Nescafe"))
		{
			//this.setVisible(false);
			pullThePlug();
			MenuScreen page = new MenuScreen("Nescafe");
		}
		else
		{
			System.out.println("Enter valid username and password");
			JOptionPane.showMessageDialog(this,"Incorrect login or password","Error",JOptionPane.ERROR_MESSAGE);
			user_text.setText("");
			pass_text.setText("");
		}
	}
}
