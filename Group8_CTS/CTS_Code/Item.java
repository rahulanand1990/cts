import java.lang.*;

class Item
{
	private String name;
	private double price;

	Item(String n, double p)
	{
		name = n;
		price = p;
	}
	
	public String getName()
	{
		return name;
	}
	public double getPrice()
	{
		return price;
	}
	public void setPrice(double price)
	{
		this.price = price;
	}
}