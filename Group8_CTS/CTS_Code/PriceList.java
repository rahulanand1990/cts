import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableCellRenderer;

import java.sql.*;


public class PriceList extends JFrame 
{
	Connection con;
	ResultSet rs;
	Statement st;
	String bn,bp;
	int bid;
    int rows = 0;
	Object data1[][];
	JScrollPane scroller;
	JTable table;
	String shopName;
	
	public void pullThePlug() 
	{
		WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
	}
	
	
    public PriceList(String s) throws Exception
    {
    	shopName = s;
	    Container cp = getContentPane();
	   			
	    //setSize(600,600);
		//setLocation(50,50);
		setTitle("Price List");
	   	
		String userName = "root";
		String password = "vishal";
		String url = "jdbc:mysql://localhost/CTS";
		Class.forName ("com.mysql.jdbc.Driver").newInstance ();
		con = DriverManager.getConnection (url, userName, password);
		
		System.out.println(con);
		
		
		System.out.println ("Database connection established");
	   	   	
		st = con.createStatement ();	//Creating Statement Object.
	    rs=st.executeQuery("SELECT * from items_"+ shopName.toLowerCase());
		rs=st.getResultSet();
		
	    while(rs.next())
	    {
	    	bid = rs.getInt(1);
	    	bn = rs.getString(2);
	    	bp = rs.getString(3);
	    	rows++;    	
	    }  
	    		
	    //System.out.println("rows = " + rows);
	    
		data1=new Object[rows][3];
			
		Object[] Colheads={"Id","Name","Price"};
		rs=st.executeQuery("SELECT * from items_"+ shopName.toLowerCase());
				
		for(int i=0;i<rows;i++)
		{
			rs.next();
			for(int j=0;j<3;j++)
			{
				data1[i][j]=rs.getString(j+1);
			}
		}
		
		JTable table=new JTable(data1,Colheads);
		table.setRowHeight(40);
		
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
		
		JScrollPane jsp=new JScrollPane(table,v,h);
		JButton logoutButton = new JButton("Logout");
		JButton backButton = new JButton("Back");			
		
		
		JPanel bottomPanel = new JPanel(new GridLayout(3,5,2,2));
		
		bottomPanel.add(new JLabel(""));
		bottomPanel.add(new JLabel(""));
		bottomPanel.add(new JLabel(""));
		bottomPanel.add(new JLabel(""));
		bottomPanel.add(new JLabel(""));
		
		bottomPanel.add(new JLabel(""));
		bottomPanel.add(backButton);
		bottomPanel.add(new JLabel(""));
		bottomPanel.add(logoutButton);
		bottomPanel.add(new JLabel(""));
		
		bottomPanel.add(new JLabel(""));
		bottomPanel.add(new JLabel(""));
		bottomPanel.add(new JLabel(""));
		bottomPanel.add(new JLabel(""));
		bottomPanel.add(new JLabel(""));
		
		
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
		table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
		table.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
		
		JPanel rootPanel = new JPanel(new GridLayout(2,1,10,10));
		rootPanel.add(jsp);
		rootPanel.add(bottomPanel);
		
		getContentPane().add(rootPanel);
		
		logoutButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
			
				JOptionPane.showMessageDialog(null,"ThankYou for using CTS");
				System.exit(0);
				
			}
		});
		
		backButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				pullThePlug();
				MenuScreen mScreen = new MenuScreen(shopName);
			}
		});
		
		pack();
		setSize(400,400);
	    setVisible(true);
		setLocationRelativeTo(null);
	}  
 }