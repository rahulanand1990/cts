import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.naming.spi.DirStateFactory.Result;


public class DatabaseHandler 
{

	String id;
	String amount;
	String shop;
	String transactionDate;
	String transactionTime;
	
	public DatabaseHandler(String i, String am, String shp)
	{
		this.id = i;
		this.amount = am;
		this.shop = shp;
	}
	
	public int writeData()
	{
		Connection conn = null;

        try
        {
			String userName = "root";
			String password = "vishal";
			String url = "jdbc:mysql://localhost/CTS";
			Class.forName ("com.mysql.jdbc.Driver").newInstance ();
			conn = DriverManager.getConnection (url, userName, password);
			System.out.println ("Database connection established");
        }
        catch (Exception e)
        {
        	System.err.println ("Cannot connect to database server");
        }
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		  
		transactionDate = dateFormat.format(date);
		  
		System.out.println("Current Date : " + transactionDate);

        DateFormat dateFormat1 = new SimpleDateFormat("HH:mm:ss");
		Date date1 = new Date();
		  
		transactionTime = dateFormat1.format(date1);
		  
		System.out.println("Current Time : " + transactionTime);

        Statement s = null;
		
        try {
		
		s = conn.createStatement ();
		    
	    String query = "INSERT INTO " + id + " VALUES('" + transactionDate + "','" + transactionTime + "','" + shop + "','" + Double.parseDouble(amount) + "');";
		String shopQuery = "INSERT INTO " + shop.toLowerCase() + "_transaction VALUES('" + transactionDate + "','" + transactionTime + "','" + id + "','" + Double.parseDouble(amount) + "');";	
	     		
		System.out.println(query);
		System.out.println(shopQuery);
		
		ResultSet rs;
		rs = s.executeQuery("SELECT * FROM student_balance where IDNo='" + id + "'");
		rs = s.getResultSet();
		
		String idno;
		String amt = null;
		
		while (rs.next()) 
		{
			idno = rs.getString(1);
			amt = rs.getString(2);
		}
		
		Double a = Double.parseDouble(amt) - Double.parseDouble(amount);
		String balanceQuery = "UPDATE student_balance SET Balance = " + a + " WHERE IDNo = '" + id + "'"; 
		int v = 0,v1 = 0,v2=0;
		
		if(Double.parseDouble(amt)>Double.parseDouble(amount))
		{
			v = s.executeUpdate(query);
			v1 = s.executeUpdate(shopQuery);
			v2= s.executeUpdate(balanceQuery);
		}
		else 
			return 2;
		
		s.close();
		
		System.out.println(v + " " + v1);
		
		if(v>=1 && v1>=1 && v2>=1)
			return 1;
		
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;

	}
		
}
