import java.lang.*;
import java.sql.SQLException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class MenuScreen extends JFrame
{
	String n_shop;
	
	JLabel shop_name;
	JButton b1,b2,b3,b4,b5;
	JPanel panel;
	
	public void pullThePlug() 
	{
		WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
	}
	
	MenuScreen(String shop)
	{
		super("Options");
		n_shop = shop;
		
		shop_name = new JLabel();
		shop_name.setText("Welcome to " + shop);
		
		b1 = new JButton("View Profile");
		b2 = new JButton("View Sales");
		b3 = new JButton("View Price List");
		b4 = new JButton("Proceed to Transaction");
		b5 = new JButton("Logout");
		
		Container contain = getContentPane();
		
		b1.addActionListener
		(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				System.out.println("To be developed");
			}
		}
		);
		
		b2.addActionListener
		(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				pullThePlug();
				DatePickerNet datePicker = new DatePickerNet(n_shop);
				//datePicker.main(null);
				
			}
		}
		);
		
		b3.addActionListener
		(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				
				try {
					pullThePlug();
					PriceList priceList = new PriceList(n_shop);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		);
		
		b4.addActionListener
		(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				//setVisible(false);
				pullThePlug();
				TransactionScreen t = new TransactionScreen(n_shop);
			}
		}
		);
		
		b5.addActionListener
		(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				JOptionPane.showMessageDialog(null,"ThankYou for using CTS");
				System.out.println("ThankYou BYE BYE!!!");
				System.exit(0);
			}
		}
		);
		
		//panel = new JPanel(new GridLayout(6,1,0,10));
		panel = new JPanel();
		panel.setLayout(null);
		
		shop_name.setBounds(85,40,150,50);
		
		b1.setBounds(50, 70, 200, 50);
		b2.setBounds(50, 135, 200, 50);
		b3.setBounds(50, 200, 200, 50);
		b4.setBounds(50, 260, 200, 50);
		b5.setBounds(50, 320, 200, 50);
		
		panel.add(shop_name);
		//panel.add(b1);
		panel.add(b2);
		panel.add(b3);
		panel.add(b4);
		panel.add(b5);
		
		contain.add(panel);
		
		setSize(300,450);
		setVisible(true);
		setLocationRelativeTo(null);
		
	}
}
			
			
			