import java.lang.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.border.LineBorder;
import javax.swing.event.*;

class TransactionScreen extends JFrame
{
    static int indexer;
    List<JLabel> listOfPriceLabels = new ArrayList<JLabel>();
    List<JLabel> listOfAmountLabels = new ArrayList<JLabel>();
    List<JComboBox> listOfQuantity= new ArrayList<JComboBox>();
    List<JComboBox> listOfItems= new ArrayList<JComboBox>();
    
	String shop;
	ImageIcon icon;
	JLabel amount_total;
	JLabel logo;
	JLabel item_name[] = new JLabel[5];
	JLabel price[] = new JLabel[5];
	JLabel amt[] = new JLabel[5];
	JPanel panel_amt, panel_items, panel_ok, panel_root,panel_img, panel_a;
	JLabel l1,l2,l3,l4;
	JLabel total;
	JButton submit,cancel,addItemsButton, backButton, logoutButton;
	JComboBox[] comboBox = new JComboBox[5];
	
	public void pullThePlug() 
	{
		WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
	}
		

	TransactionScreen(String s)
	{
		
		super("CTS @ " + s);
		indexer=1;
		shop = s;
		amount_total = new JLabel("0.0",JLabel.CENTER);
		amount_total.setFont(new Font(null, Font.BOLD, 30));
		
		String[] quantity = { "0", "1", "2", "3", "4" };
		
		int i;
		
		l1 = new JLabel("Item",JLabel.CENTER);
		l2 = new JLabel("Quantity");
		l3 = new JLabel("Price",JLabel.CENTER);
		l4 = new JLabel("Amount");
		total = new JLabel("Total Amount",JLabel.CENTER);
		total.setFont(new Font(null, Font.PLAIN, 30));
		submit = new JButton("Submit");
		submit.setFont(new Font(null,Font.BOLD,20));
		cancel = new JButton("Cancel");
		cancel.setFont(new Font(null,Font.BOLD,20));
		addItemsButton = new JButton("Add");
		addItemsButton.setFont(new Font(null,Font.BOLD,20));
		backButton = new JButton("Back");
		backButton.setFont(new Font(null,Font.BOLD,20));
		logoutButton = new JButton("Logout");
		logoutButton.setFont(new Font(null,Font.BOLD,20));
		
		submit.addActionListener
		(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				//setVisible(false);
				pullThePlug();
				//StudentPage sp = new StudentPage(amount_total.getText(),s);
			}
		}
		);
		
		cancel.addActionListener
		(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{

				pullThePlug();
				MenuScreen screen = new MenuScreen(shop);
				
			}
		}
		);
		
		
		/*for(i=0; i<5; i++)
		{
			item_name[i] = new JLabel("0.0",JLabel.CENTER);
			price[i] = new JLabel("0.0",JLabel.CENTER);
			amt[i] = new JLabel("0.0");
			amt[i].setFont(new Font(null, Font.BOLD, 20));
			
			comboBox[i] = new JComboBox(quantity);
			comboBox[i].setActionCommand(i + "");
			comboBox[i].setSelectedIndex(0);
		}*/		
	
		
		//panel_items = new JPanel(new GridLayout(6,4,20,20));
		
		panel_items = new JPanel(new GridBagLayout());
		//panel_items.setPreferredSize(new Dimension(200,200));
		//panel_items.setBorder(LineBorder.createBlackLineBorder());
		
		
		panel_root = new JPanel(new GridBagLayout());
		//panel_root.add(panel_amt);
		//panel_root.add(panel_items);
		//panel_root.add(panel_ok);
		
        GridBagConstraints frameConstraints = new GridBagConstraints();                
        
        frameConstraints.gridx = 0;
        frameConstraints.gridy = 0;
        frameConstraints.insets = new Insets(60, 10, 10, 10);
        panel_root.add(total, frameConstraints);
        
        frameConstraints.gridx = 2;
        frameConstraints.gridy = 0;
        panel_root.add(amount_total, frameConstraints);
        
        frameConstraints.gridx = 1;
        frameConstraints.gridy = 1;
        panel_root.add(panel_items, frameConstraints);
        
        frameConstraints.gridx = 0;
        frameConstraints.gridy = 2;
        frameConstraints.insets = new Insets(30, 20, 10, 10);
        panel_root.add(addItemsButton, frameConstraints);        
        
        frameConstraints.gridx = 2;
        frameConstraints.gridy = 2;
        frameConstraints.insets = new Insets(30, 40, 10, 10);
        panel_root.add(submit, frameConstraints);
        
		frameConstraints.gridx = 2;
		frameConstraints.gridy = 2;
		frameConstraints.insets = new Insets(30, 40, 10, 10);
		//panel_root.add(cancel, frameConstraints);
        
        frameConstraints.gridx = 0;
		frameConstraints.gridy = 3;
		frameConstraints.insets = new Insets(10, 10, 10, 10);
		panel_root.add(cancel, frameConstraints);
        
        frameConstraints.gridx = 2;
		frameConstraints.gridy = 3;
		frameConstraints.insets = new Insets(10, 40, 10, 10);
		panel_root.add(logoutButton, frameConstraints);
        				
		JScrollPane scrollPane = new JScrollPane(panel_root, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);		
		
        Container contain = getContentPane();
		contain.add(scrollPane);
		
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		setSize(800,750);
		setVisible(true);
		setLocationRelativeTo(null);
		
		
		addItemsButton.addActionListener(new AddItemsListener());
		submit.addActionListener(new SubmitListener());
		
		
		backButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
			
				pullThePlug();
				MenuScreen menuScreen = new MenuScreen(shop);
				
			}
		});
		
		logoutButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
			
				JOptionPane.showMessageDialog(null, "Thank You for using CTS !!");
				System.exit(0);
			}
		});
	}
	
	private class AddItemsListener implements ActionListener
	{

		@Override
        public void actionPerformed(ActionEvent arg0) 
        {       
            // Clear panel
            
			panel_items.removeAll();

            String[] shopItems = new String[5];
            
            if(shop.equals("Monginis"))
            {
            	Monginis mon = new Monginis(shop);
            	
            	for(int k=0;k<5;k++)
            	{
            		shopItems[k] = new String();
            		shopItems[k] = mon.items[k].getName();            	
            	}
            }
            else if(shop.equals("Nescafe"))
            {
            	Nescafe nes = new Nescafe(shop);
            	
            	for(int k=0;k<5;k++)
            	{
            		shopItems[k] = new String();
            		shopItems[k] = nes.items[k].getName();
            	}
            } 
                                  
            JComboBox comboItem = new JComboBox(shopItems);
            listOfItems.add(comboItem);
            comboItem.setActionCommand(indexer + "");            
            comboItem.setFont(new Font(null, Font.PLAIN, 14));
            comboItem.setSelectedIndex(0);
            
            String[] quantity = { "0", "1", "2", "3", "4" };
            JComboBox comboQuantity = new JComboBox(quantity);
            listOfQuantity.add(comboQuantity);
            comboQuantity.setActionCommand(indexer + "");
            comboQuantity.setFont(new Font(null, Font.PLAIN, 14));
                              
            String p = null;
            if(shop.equals("Monginis"))
            {
            	Monginis mon = new Monginis(shop);
            	p = String.valueOf(mon.items[0].getPrice());
            }
            else if(shop.equals("Nescafe"))
            {
            	Nescafe nes = new Nescafe(shop);
            	p = String.valueOf(nes.items[0].getPrice());
            }
            
            JLabel priceLabel = new JLabel(p);
            listOfPriceLabels.add(priceLabel);
            priceLabel.setFont(new Font(null, Font.BOLD, 14));
            
            JLabel amountLabel = new JLabel("0.0");
            listOfAmountLabels.add(amountLabel);
            amountLabel.setFont(new Font(null, Font.BOLD, 14));
            
            GridBagConstraints comboQuantityConstraints = new GridBagConstraints();
            GridBagConstraints comboItemConstraints = new GridBagConstraints();
            GridBagConstraints priceLabelConstraints = new GridBagConstraints();
            GridBagConstraints amountLabelConstraints = new GridBagConstraints();
            
            GridBagConstraints gbc = new GridBagConstraints();
            GridBagConstraints gbc1 = new GridBagConstraints();
            GridBagConstraints gbc2 = new GridBagConstraints();
            GridBagConstraints gbc3 = new GridBagConstraints();
            
            // Add labels and text fields
            for(int i = 0; i < indexer; i++)
            {
            	gbc.gridx = 0;
            	gbc.gridy = 0;
            	gbc.insets = new Insets(10,10,10,20);
            	
            	gbc1.gridx = 1;
            	gbc1.gridy = 0;
            	gbc1.insets = new Insets(10,10,10,20);            	
            	
            	gbc2.gridx = 2;
            	gbc2.gridy = 0;
            	gbc2.insets = new Insets(10,10,10,20);
            	            
            	gbc3.gridx = 3;
            	gbc3.gridy = 0;
            	gbc3.insets = new Insets(10,10,10,20);
            	
             	comboItemConstraints.gridx = 0;
            	comboItemConstraints.gridy = i+1;
            	//comboItemConstraints.gridwidth = 2;
            	comboItemConstraints.insets = new Insets(10, 10, 10, 20);
            	
            	comboQuantityConstraints.gridx = 1;
            	comboQuantityConstraints.gridy = i+1;
            	comboQuantityConstraints.insets = new Insets(10, 10, 10, 20);
            	//comboQuantityConstraints.fill = GridBagConstraints.HORIZONTAL;
            	
            	priceLabelConstraints.gridx = 2;
            	priceLabelConstraints.gridy = i+1;
            	priceLabelConstraints.insets = new Insets(10, 10, 10, 20);
            	//priceLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            	
            	amountLabelConstraints.gridx = 3;
            	amountLabelConstraints.gridy = i+1;        
            	amountLabelConstraints.insets = new Insets(10, 10, 10, 20);
            	//amountLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            	
                // Add them to panel
            	    
            	panel_items.add(new JLabel("Item"),gbc);
            	panel_items.add(new JLabel("Quantity"),gbc1);
            	panel_items.add(new JLabel("Price"),gbc2);
            	panel_items.add(new JLabel("Amount"),gbc3);
            	
            	panel_items.add(listOfItems.get(i), comboItemConstraints);
            	panel_items.add(listOfQuantity.get(i), comboQuantityConstraints);
            	panel_items.add(listOfPriceLabels.get(i), priceLabelConstraints);
            	panel_items.add(listOfAmountLabels.get(i),amountLabelConstraints);
            	                	
            	panel_items.revalidate();
            	
            }
        	
            // Align components top-to-bottom
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = indexer;
            c.weighty = 1;
            //c.insets = new Insets(10,10,10,10);
            panel_items.add(new JLabel(), c);                            
            
            // Increment indexer
            indexer++;
            panel_items.updateUI();
            
            for(int i=0;i<indexer-1;i++)
            {            	
            	JComboBox qty = (JComboBox) listOfQuantity.get(i);            	
            	JComboBox it = (JComboBox) listOfItems.get(i);
            	
            	it.addActionListener(new ItemListener());            	
            	qty.addActionListener(new QuantityListener());
					
            	
            }
        }
		
	}
	
	private class ItemListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub

			String i = arg0.getActionCommand();
			//System.out.println("i value item = " + i);
			
			JLabel prLabel = (JLabel) listOfPriceLabels.get(Integer.valueOf(i)-1);
			JLabel amtLabel = (JLabel) listOfAmountLabels.get(Integer.valueOf(i)-1);
			
			JComboBox cb = (JComboBox)arg0.getSource();
			int id = cb.getSelectedIndex();
			//System.out.println(cb.getSelectedItem());
			
			if(shop.equals("Monginis"))
			{				
				Monginis mon = new Monginis(shop);
				//icon = new ImageIcon("C:\\Users\\Vishal\\Java Workspace\\CTS\\bin\\mongilogo.jpg");
												
				//System.out.println(mon.items[id].getPrice());
				
				prLabel.setText(String.valueOf(mon.items[id].getPrice()));				
				JComboBox q = (JComboBox) listOfQuantity.get(Integer.valueOf(i)-1);
				String qq = (String) q.getSelectedItem();
				amtLabel.setText(String.valueOf(Double.parseDouble(qq) * Double.parseDouble(prLabel.getText())));
				
			}
			else if(shop.equals("Nescafe"))
			{
				Nescafe nesc = new Nescafe(shop);
				//icon = new ImageIcon("C:\\Users\\Vishal\\Java Workspace\\CTS\\bin\\mongilogo.jpg");
												
				prLabel.setText(String.valueOf(nesc.items[id].getPrice()));				
				JComboBox q = (JComboBox) listOfQuantity.get(Integer.valueOf(i)-1);
				String qq = (String) q.getSelectedItem();
				amtLabel.setText(String.valueOf(Double.parseDouble(qq) * Double.parseDouble(prLabel.getText())));
							
			}			
			
			double total = 0;
			
			for(int b=0; b<indexer-1; b++)
			{
				JLabel amt = (JLabel)listOfAmountLabels.get(b);
				//System.out.println(amt.getText());
				total = total + Double.parseDouble(amt.getText());
			}
			
			amount_total.setText(String.valueOf(total));
		}
		
	}
	
	private class QuantityListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0) 
		{
			// TODO Auto-generated method stub
		
			String i = arg0.getActionCommand();
			//System.out.println("i value qty = " + i);
			
			JLabel amtLabel = (JLabel) listOfAmountLabels.get(Integer.valueOf(i)-1);
			JLabel prLabel = (JLabel) listOfPriceLabels.get(Integer.valueOf(i)-1);

			JComboBox cb = (JComboBox)listOfItems.get(Integer.valueOf(i)-1);			
			int id = cb.getSelectedIndex();
		
			if(shop.equals("Monginis"))
			{
				Monginis mon = new Monginis(shop);
				//icon = new ImageIcon("C:\\Users\\Vishal\\Java Workspace\\CTS\\bin\\mongilogo.jpg");
												
				prLabel.setText(String.valueOf(mon.items[id].getPrice()));
				
				JComboBox q = (JComboBox) arg0.getSource();
				String qq = (String) q.getSelectedItem();
				amtLabel.setText(String.valueOf(Double.parseDouble(qq) * Double.parseDouble(prLabel.getText())));
							
			}			
			else if(shop.equals("Nescafe"))
			{
				Nescafe nesc = new Nescafe(shop);
				//icon = new ImageIcon("C:\\Users\\Vishal\\Java Workspace\\CTS\\bin\\mongilogo.jpg");
												
				prLabel.setText(String.valueOf(nesc.items[id].getPrice()));
				
				JComboBox q = (JComboBox) arg0.getSource();
				String qq = (String) q.getSelectedItem();
				amtLabel.setText(String.valueOf(Double.parseDouble(qq) * Double.parseDouble(prLabel.getText())));
							
			}			

			double total = 0;
			
			for(int b=0; b<indexer-1; b++)
			{
				JLabel amt = (JLabel)listOfAmountLabels.get(b);
				//System.out.println(amt.getText());
				total = total + Double.parseDouble(amt.getText());
			}
			
			amount_total.setText(String.valueOf(total));

			
		}
		
	}
	
	private class SubmitListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0) 
		{
			// TODO Auto-generated method stub
			
			//pullThePlug();
			if(!(amount_total.getText().equals("0.0")))
			{
				StudentPage studentPage = new StudentPage(amount_total.getText(),shop);
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Please select something to buy !!!");
				pullThePlug();
				TransactionScreen tScreen = new TransactionScreen(shop);
			}
			
		}
		
	}
	
		
}